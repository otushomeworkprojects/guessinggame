﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    public  class GuessingGameProcess : IGameProcess
    {
        private ISettingsGetter _settingsGetter { get; set; }

        public GuessingGameProcess(ISettingsGetter settingsGetter)
        {
            _settingsGetter = settingsGetter;
        }
     
        public void Start() {
            if (_settingsGetter.IsGuessGameSettings())
            {
                Console.WriteLine($"Количество возможных попыток: {_settingsGetter.GetAttemptsCount()}");
                var random = new Random();
                var next = random.Next(_settingsGetter.GetNumberRange());
                for (int i = 1; i <= _settingsGetter.GetAttemptsCount(); i++)
                {
                    Console.WriteLine($"Попытка №{i}");
                    var number = Convert.ToInt32(Console.ReadLine());
                    if (number == next)
                    {
                        Console.WriteLine("Победа!");
                        return;
                    }
                    if (number < next)
                    {
                        Console.WriteLine($"{number} < n");
                    }
                    if (number > next)
                    {
                        Console.WriteLine($"{number} > n");
                    }
                }

                Console.WriteLine("Проигрыш :(");
            }
        }
    }
}
