﻿using GuessingGame;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((_, services) =>
        services.AddSingleton<IGameProcess, GuessingGameProcess>()
        .AddSingleton<ISettingsGetter, SettingsGetter>()
        .AddTransient<GuessGameSettings>())
.Build();

using IServiceScope serviceScope = host.Services.CreateScope();
IServiceProvider provider = serviceScope.ServiceProvider;

var process = provider.GetRequiredService<IGameProcess>();
process.Start();


await host.RunAsync();