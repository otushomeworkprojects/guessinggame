﻿using Microsoft.Extensions.Configuration;
using System.Configuration;

namespace GuessingGame
{
    public class GameSettings : Settings
    {
        public int AttemptsCount { get; private set; }
        public int NumberRange { get; private set; }

        public override void GetSettings()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false)
                .Build(); 
            AttemptsCount = config.GetValue<int>("AttemptsCount");
            NumberRange = config.GetValue<int>("NumberRange");
        }
    }
}
