﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    public class SettingsGetter : ISettingsGetter
    {
        private GameSettings _settings;
        public SettingsGetter(GuessGameSettings settings) {
            settings.GetSettings();
            _settings = settings; }

        public int GetAttemptsCount()
        {
            return _settings.AttemptsCount;
        }

        public int GetNumberRange()
        {
            return _settings.NumberRange;
        }

        public bool IsGuessGameSettings()
        {
            return _settings is GuessGameSettings;
        }
    }
}
