# GuessingGame

Игра "Угадай число".
Программа рандомно генерирует число, пользователь должен угадать это число. При каждом вводе числа программа пишет больше или меньше отгадываемого. Кол-во попыток отгадывания и диапазон чисел должен задаваться из настроек.

Использование SOLID:

1. Single Responsibility Principle - Каждый класс отвечает только за одно действие (Program - запускает программу, GuessingGameProcess - логика игры, GameSettings - настройки игры)
2. Open/Closed Principle - Использование интерфейса IGameProcess
3. Liskov Substitution Principle - Наследование GuessGameSettings от GameSettings. Здесь уже фантазии не хватило(чем отличить GuessGameSettings)
4. Interface Segregation Principle - также на примере IGameProcess
5. Dependency Inversion Principle - класс Program становится зависим от IGameProcess
